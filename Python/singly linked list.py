'''
Singly Linked Lists store data in memory which are not in contiguous locations
Each node contains data and a pointer

Advantages over arrays:
1. Dynamic size
2. Ease of insertion/deletion

Disadvantages:
1. No random access
2. Extra memory required for pointer for each element
3. No caching, since locations are not contiguous

Time Complexity
Access O(n)
Search O(n)
Insertion O(1)
Deletion O(1)

Space Complexity
O(n)
'''

# A simple Python program to introduce a linked list 

# Node class 
class Node: 

	# Function to initialise the node object 
	def __init__(self, data): 
		self.data = data # Assign data 
		self.next = None # Initialize next as null 


# Linked List class contains a Node object 
class LinkedList: 

	# Function to initialize head 
	def __init__(self): 
	    self.head = None
        
    # This function prints contents of linked list 
    # starting from head 
	def printList(self):
		temp = self.head 
		while (temp): 
			print(temp.data) 
			temp = temp.next

# Code execution starts here 
if __name__=='__main__': 

	# Start with the empty list 
	llist = LinkedList() 

	llist.head = Node(1) 
	second = Node(2) 
	third = Node(3) 

	''' 
	Three nodes have been created. 
	We have references to these three blocks as first, 
	second and third 

	llist.head	 second			 third 
		|			 |				 | 
		|			 |				 | 
	+----+------+	 +----+------+	 +----+------+ 
	| 1 | None |	 | 2 | None |	 | 3 | None | 
	+----+------+	 +----+------+	 +----+------+ 
	'''

	llist.head.next = second # Link first node with second 

	''' 
	Now next of first Node refers to second. So they 
	both are linked. 

	llist.head	 second			 third 
		|			 |				 | 
		|			 |				 | 
	+----+------+	 +----+------+	 +----+------+ 
	| 1 | o-------->| 2 | null |	 | 3 | null | 
	+----+------+	 +----+------+	 +----+------+ 
	'''

	second.next = third # Link second node with the third node 

	''' 
	Now next of second Node refers to third. So all three 
	nodes are linked. 

	llist.head	 second			 third 
		|			 |				 | 
		|			 |				 | 
	+----+------+	 +----+------+	 +----+------+ 
	| 1 | o-------->| 2 | o-------->| 3 | null | 
	+----+------+	 +----+------+	 +----+------+ 
	'''

	llist.printList()