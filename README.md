# Data structures

This repo will explore the fundamentals behind data structure creation.

The following data structures will be explored in this repo:

- Array
- Stack
- Queue
- Singly-Linked List
- Doubly-Linked List
- Skip List
- Hash Table
- Binary Search Tree
- Cartesian Tree
- B-Tree
- Red-Black Tree
- Splay Tree
- AVL Tree
- KD Tree